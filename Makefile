###############################################################################
# MAKE CONFIG

MAKEFLAGS+=--warn-undefined-variables
MAKEFLAGS+=--no-builtin-rules

###############################################################################

SITE_CONFIG=src/conf/site.mk
SITE_DB=src/conf/site.db

# Pull the user's config, which will be injected into pandoc's compilation
# process; no checks are performed so if all required variables in the
# SITE_CONFIG file are not specified, the output is undefined.
include ${SITE_CONFIG}

DATE=$(shell date +${DATE_FMT})

###############################################################################
# INPUT/OUTPUT FILES

SRC_ROOT=src
SRC_DIR=${SRC_ROOT}/base

DST=build
DST_DIR=${DST}/www
DIST=${DST}/dist
SITE_TREE=${DST}/templates/site-tree.html

# Template files used to generate the site.
CSS_TEMPLATE?=defaults/templates/tmpl.css
HTML_TEMPLATE?=defaults/templates/tmpl.html
INDEX_HTML_TEMPLATE?=defaults/templates/index.tmpl.html

# Consider every markdown file in SRC_DIR, and for each markdown file, generate
# its correspondng HTML and CSS.
SRCS=$(shell find ${SRC_DIR} -name '*.md')
OUT_HTML=${SRCS:${SRC_DIR}/%.md=${DST_DIR}/%.html}
OUT_CSS=${SRCS:${SRC_DIR}/%.md=${DST_DIR}/%.css}

# Consider every HTML file in SRC_DIR, and for each file, copy it over to the
# build directory, without any changes.
SRCS_HTML=$(shell find ${SRC_DIR} -name '*.html')
OUT_HTML_UNPROCESSED=${SRCS_HTML:${SRC_DIR}/%.html=${DST_DIR}/%.html}

# Consider every CSS file in SRC_DIR, and for each file, copy it over to the
# build directory, without any changes.
SRCS_CSS=$(shell find ${SRC_DIR} -name '*.css')
OUT_CSS_UNPROCESSED=${SRCS_CSS:${SRC_DIR}/%.css=${DST_DIR}/%.css}

ASSETS=$(shell sh -c "[ -d ${SRC_ROOT}/assets ] && find ${SRC_ROOT}/assets -name '*'")
OUT_ASSETS=${ASSETS:${SRC_ROOT}/assets/%=${DST_DIR}/assets/%}

ROBOTS_OUT=${DST_DIR}/robots.txt

###############################################################################
# DEPENDANCIES & SCRIPTS

# Using a statically-linked pre-compiled build of pandoc to generate HTML.
# The version in the Arch repos has an entire army of dependencies which can
# really take up some space.
#
# GitHub will redirect all requests to the /releases/latest path to a specific
# tag which, at the time, is the latest; sed is simply being used to extract the
# latest version number, which is then used to form the URL to download the
# pre-compiled build of pandoc.
PANDOC_VERSION=$(shell curl -s https://github.com/jgm/pandoc/releases/latest | sed 's/.*tag\/// ; s/">.*//')
ifeq ($(shell uname -s), Linux)
	PANDOC_OS_VARIANT=linux-amd64.tar.gz
endif
ifeq ($(shell uname -s), Darwin)
	PANDOC_OS_VARIANT=macOS.zip
endif
PANDOC_URL=https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-${PANDOC_OS_VARIANT}
PANDOC=bin/pandoc
PANDOC_OPTS=${DST}/pandoc-opts.yaml

GEN_SITE_TREE=bin/gen-site-tree
GEN_ROBOTS_FILE=bin/gen-robots-file
GEN_PANDOC_OPTS=bin/gen-pandoc-opts

SRC_TREE_HASH=${DST}/src-tree-hash

###############################################################################
# TARGETS
###############################################################################

.PHONY: all dist clean serve

# generate entire site
all: ${SITE_TREE} ${PANDOC_OPTS} \
	${OUT_HTML} ${OUT_CSS} \
	${OUT_HTML_UNPROCESSED} ${OUT_CSS_UNPROCESSED} \
	${OUT_ASSETS} ${ROBOTS_OUT}

clean:
	rm -rf ${DST}

dist: all
	@echo "==> [info] generating tarball..."
	mkdir -p ${DIST}
	tar -uf ${DIST}/$(shell date +'%d-%m-%Y-%H-%M').tar.gz ${DST_DIR}
	@echo "==> [info] done"

serve: all
	python3 -m http.server --directory ${DST_DIR}/

###############################################################################

# If pandoc has not been downloaded, download it and keep it around.
${PANDOC}:
	@mkdir -p "${@D}"
	@curl -L -O ${PANDOC_URL}
ifeq ($(shell uname -s), Linux)
	@tar -xf pandoc-${PANDOC_VERSION}-${PANDOC_OS_VARIANT}
endif
ifeq ($(shell uname -s), Darwin)
	@unzip pandoc-${PANDOC_VERSION}-${PANDOC_OS_VARIANT}
endif
	@mv pandoc-${PANDOC_VERSION}/bin/* "${@D}"
	@rm -rf pandoc-${PANDOC_VERSION} pandoc-${PANDOC_VERSION}-${PANDOC_OS_VARIANT}

###############################################################################

${SRC_ROOT}: src-git
	mkdir -p "${@D}"
	git clone "$(shell cat src-git)" ${SRC_ROOT}

# Generate the CSS template, if it doesn't exist.
${PANDOC_CSS_TEMPLATE}: ${PANDOC}
	@mkdir -p "${@D}"
	${PANDOC} --print-default-data-file=templates/styles.html > $@

# Generate the HTML template, if it doesn't exist.
${PANDOC_HTML_TEMPLATE}: ${PANDOC}
	@mkdir -p "${@D}"
	${PANDOC} --print-default-template=html5 > $@

${PANDOC_OPTS}: ${GEN_PANDOC_OPTS} ${SITE_CONFIG} Makefile
	@echo "==> [info] generating pandoc options..."
	mkdir -p "${@D}"
	DST="${DST}" ./$< "${SITE_CONFIG}" "$@"
	@echo "==> [info] done"

${SRC_TREE_HASH}: ${SRCS} ${SRCS_HTML}
	@echo "==> [info] checking if site map has changed..."
	$(shell sh -c 'mkdir -p ${@D} ; ls ${SRC_DIR}/**/* > $@.new ; [ ! -f "$@" ] && cp $@.new $@ ; diff -q "$@" "$@.new"; if [[ $$? -ne 0 ]]; then mv "$@.new" "$@"; else rm "$@.new"; fi')

${SITE_TREE}: ${GEN_SITE_TREE} ${SRC_TREE_HASH}
	@echo "==> [info] recreating site-tree"
	mkdir -p "${@D}"
	./$< "$@"
	@echo "==> [info] done"

${ROBOTS_OUT}: ${GEN_ROBOTS_FILE} ${SITE_CONFIG}
	@echo "==> [info] generating $@"
	mkdir -p "${@D}"
	HOST=${HOST} ./$< "$@" "${ROBOTS_DISALLOW}"
	@echo "==> [info] done"

${DST_DIR}/assets/%: ${SRC_ROOT}/assets/%
	mkdir -p "${@D}"
	cp -f "$<" "$@"

###############################################################################

${DST_DIR}/index.html: ${SRC_DIR}/index.md ${INDEX_HTML_TEMPLATE} ${PANDOC_OPTS} ${PANDOC}
	@mkdir -p "${@D}"
	@echo "==> [info] compiling '$<' to HTML"
	${PANDOC} $< -t html -o $@ \
		--template=${INDEX_HTML_TEMPLATE}         \
		-V 'date:${DATE}'                  \
		-V 'css:${@:${DST_DIR}%html=%css}' \
		--defaults=${PANDOC_OPTS}

# How to make an HTML file from a (pandoc-flavoured) Markdown one.
# This does not depend on the Makefile so that the dates on the generated site
# are correct; otherwise every time the Makefile is updated, all documents will
# be re-compiled and will appear to have been updated.
${DST_DIR}/%.html: ${SRC_DIR}/%.md ${HTML_TEMPLATE} ${PANDOC_OPTS} ${PANDOC} ${SITE_TREE}
	@mkdir -p "${@D}"
	@echo "==> [info] compiling '$<' to HTML"
	$(eval WC:=$(shell wc -w "$<" | cut -d' ' -f1 | xargs -I % sh -c 'echo %/100\*100+100 ' | bc))
	${PANDOC} $< -t html -o $@ --toc     \
		--template=${HTML_TEMPLATE}        \
		-V 'date:${DATE}'                  \
		-V 'css:${@:${DST_DIR}%html=%css}' \
		-V 'wordcount:${WC}'               \
		--defaults=${PANDOC_OPTS}

	@echo "==> [info] upadting tags info for '$@'"
	$(shell [ ! -f "${SITE_DB}" ] && printf '{}' > "${SITE_DB}")
	$(eval TAGS:=$(shell sh -c "grep '^keywords: ' < $< | sed -e 's/keywords:.*\[//g ; s/]//' | tr -d ' '"))
	jq                                                                              \
		--arg slug "${@:${DST_DIR}/%=%}"                                              \
		--arg tags "${TAGS}"                                                          \
		--arg date "${DATE}"                                                          \
		'.posts[$$slug]?.tags=$$tags | .posts[$$slug]?.metadata?.last_updated=$$date' \
		${SITE_DB} > ${SITE_DB}.new
	mv ${SITE_DB} ${SITE_DB}.bak
	mv ${SITE_DB}.new ${SITE_DB}

# Generate a stylesheet for the given Markdown file.
# Pandoc's generated generated CSS depends on the Markdown language
# features/extensions used (e.g. math, code); it should be the minimum set of
# style rules required to make the page appear as intended.
#
# Common metadata is injected into each file's compilation process directly over
# the command line.
${DST_DIR}/%.css: ${SRC_DIR}/%.md ${CSS_TEMPLATE} ${PANDOC} ${PANDOC_OPTS} Makefile
	mkdir -p "${@D}"
	@echo "==> [info] generating CSS for '$<'"
	${PANDOC} $< -t html -o $@    \
		--template=${CSS_TEMPLATE}  \
		--defaults=${PANDOC_OPTS}

# Copy over HTML source without any processing.
${DST_DIR}/%.html: ${SRC_DIR}/%.html Makefile
	@echo "==> [info] using HTML source '$<' as-is"
	@mkdir -p "${@D}"
	cp -f $< $@

# Copy over CSS source without any processing.
${DST_DIR}/%.css: ${SRC_DIR}/%.css Makefile
	@echo "==> [info] using CSS source '$<' as-is"
	@mkdir -p "${@D}"
	cp -f $< $@
