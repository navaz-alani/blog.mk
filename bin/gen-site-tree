#!/bin/bash

[ ! -d src/base ] && \
  echo "==> [error] content root '${BASE}' not found" && \
  exit 1
BASE=src/base

exec 3>&1     # backup copy of stdout
exec 1>"${1}" # redirect stdout output to file

###############################################################################
# HTML generation

printf '<div class="site-tree-container">'
printf '<h6 id="site-tree-toggle" title="Site Map">Site 🔍 Map</h6>'
printf '<ul id="site-tree">\n'

printf '  <li class="subtree-file"><a class="internal-link" href="/">Home 🏠</a></li>\n'

# Recurively (depth-first) walk the directory structure (in which it is assumed
# there are no links), and emit a site-tree in the process.
process_directory() {
  local base="${1}"
  local pre="${2}"

  local files=(`ls "${base}"`)

  local only_directories=true
  for f in "${files[@]}"; do
    [ ! -d "${base}/${f}" ] && only_directories=false && break
  done
  if [ -z "${3}" ]; then
    if [ ! ${only_directories} ]; then
      printf '%s<ul class="subtree-nested">\n' "${pre}"
    else
      printf '%s<ul class="subtree-nested">\n'
    fi
  fi

  for f in ${files[@]}; do
    case "${f}" in
      __test.md|index.md)
        continue ;;
    esac

    case "${f#*.}" in
      css)
        continue ;;
    esac
    name="${f%.*}"

    if [ -d "${base}/${f}" ]; then
      if [ -z "${3}" ] && [ ! ${only_directories} ]; then
        printf '%s<li class="subtree-nested"><span class="subtree-caret">%s</span>\n' \
          "  ${pre}" "${name^}"
      else
        printf '%s<li><span class="subtree-caret">%s</span>\n' \
          "  ${pre}" "${name^}"
      fi
      process_directory "${base}/${f}" "    ${pre}"
      printf '%s</li>\n' "  ${pre}"
    else
      link="${base##$BASE}"
      printf '%s<li class="subtree-file"><a class="internal-link" href="%s">%s</a></li>\n' \
        "  ${pre}" \
        "${base##${BASE}}/${f%%.*}.html" \
        "`grep '^title: ' "${base}/${f}" -m 1 | sed 's/^title:\s*//'`"
    fi
  done

  [ -z "${3}" ] && printf '%s</ul>\n' "${pre}"
}

process_directory "${BASE}" "  " "top-level"

printf '</ul></div>\n'

###############################################################################

exec 3>&1 # restore stdout
exec 3>&- # close copy of stdout
