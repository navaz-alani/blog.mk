# blog.md

This is a simple static site generator, powered by pandoc.
Given an input directory tree structure `src` containing markdown files
(technically, any files that pandoc can work with), blog.md converts the entire
tree into a static HTML site which has the same tree structure as the source
tree.

## Source Tree

Next, to use `blog.md`, a __source tree__ is required.
A source tree is any directory which has the following format

```
.
├── assets                   # e.g. images
│   ├── ...
│   └── ...
├── base                     # base directory for content
│   ├── index.md
│   ├── [category]
│   │   ├── [sub-category]
│   │   │   ├── ...
│   │   │   └── ... 
│   │   └── ...
│   ├── post_1.md
│   ├── ...
│   └── __test.md            # test page
└── conf
    └── site.mk
```

A few important notes about the source tree:

* Since a Makefile is going to be orchestrating the site compilation, one has to
  make sure that files names in the source tree don't contain spaces, colons,
  dollar signs, etc.
  In general, any characters which Makefiles have a hard time dealing with/need
  escaping should not be used in file names.

## Getting Started

First, clone this repository.

It is recommended that the source tree be its own repository, and is sym-linked
to this repository, as `src` like so:
```bash
# run this in this repositorys root
ln -s /path/to/source-tree src
```
This way, this project and the source tree can be version controlled separately.

A few notes about the compilation/build process:

* File at path `src/base/[x]` maps to path `build/www/[x]` in the build
  directory and `/[x]` on the generated site.
* Only markdown files in the base directory are compiled to HTML.
* HTML and CSS files in the base directory and the assets' directory are
  replicated in the build directory as-is.
* A source tree must have a `conf/site.mk` file which is used to configure the
  generated site (an example one is found in `defaults/conf/site.mk.example`).
* `__test.md` is a file which, if provided, is included in the generated site as
  test page (disallowed in robots file, and has no links pointing to it by any
  generated code).
  A sample test post is in `defaults/__test.md`.

## Development

* `make` will compile the source tree; after the first build, only files which
  need to be re-compiled are re-compiled.
* One can set up their own local branch in this repository, and make changes to
  modify the build process, etc.
  Updates can be received by merging the `master` branch of this repository into
  one's local branch and resolving any conflicts that arise.
* `make serve` will generate the site and begin serving up the static files on
  `0.0.0.0:8000`.
  After modifying a file, you can just run `make` to re-build the file; the
  server will automatically pick this up.

## Deployment

`make dist` will generate a single `.tar.gz` export of the built site in the
`dist` directory; the generated file names include the time/date of the build.
