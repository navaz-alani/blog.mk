---
title: Test Page
subtitle: If this page looks good, most things on the site should be fine.
abstract: |
  The page you are currently viewing uses a broad set of language features (of
  the pandoc-flavored markdown) to ensure that the generated site looks good.
keywords: [testing, mathjax, pandoc]
---


## Mathematics

### Inline

If an equation has the form $ax^2 + bx + c$, then the quadratic formula can be
used to solve for $x$.
Recall that the quadratic formula say that $x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}$.
Note that $x$ need not be a scalar value -- it could even be a function.

### Display

If all is good so far, it means both inline and display math modes are working
as expected.
Let's try to show some more complicated-looking inline math, which changes the
default spacing of the lines. 
Recall that $$ R = \begin{pmatrix}
  \cos\theta & -\sin\theta \\
  \sin\theta & \cos\theta
\end{pmatrix} $$ is called the __rotation matrix__ because, its corresponding
linear transformation (when applied to $\mathbb{R}^2$) has the effect of
rotating the plane by $\theta$ degrees.
This can be shown with geometric arguments.

## Code

Now, let us turn our attention to code.
I would like the site to be able to handle both `inline code` and code blocks.
Have a look at the following "Hello, World" program in Intel-style x86-64 64-bit
assembly:
```nasm
section .text
    global  _start

_start:
  mov     rdx,str_len ; number of bytes to write
  mov     rsi,str     ; string to print
  mov     rdi,1       ; stdout
  mov     rax,1       ; sys_write
  syscall

  mov     rdi,0       ; exit code
  mov     rax,60      ; sys_exit
  syscall

section .data
  str: db "hello, world!",0xa,0
  str_len: equ $ - str
```

Assuming the code is stored in a file called 'main.asm', here is how it would
be compiled and run the following sequence of commands (assuming you have
`nasm`, and a linker installed on your system):

```bash
 nasm -felf64 main.asm -o main.o
 ld main.o -o main
 ./main # prints 'hello, world!'
```

The code blocks above should have syntax highlighting which distinguishes the
tokens of the code (e.g. comments slightly grayed out, literals and scalars are
highlighted, etc.).
Additionally, it should also be easy to read -- the contrast should be easy on
the eye.

## Tables

I think when I was new to $\LaTeX$, I didn't know about the `*matrix` math
environments, so I weirdly used tables to typeset matrices for a while before
realizing that `pmatrix` is so much easier.
Anyway, here is a table that has no meaning.

| C1 | C2 | C3 |
|----|----|----|
| X  | Y  | Z  |
| A  | B  | C  |
| P  | Q  | R  |

## Lists

Here is a contrived list, which tests nesting levels.

- Item 1
  * Item 1A
    - Item 1A1
      - Item 1A1A
* Item 2
* Item 3

## TODO lists

- [x] completed item
  - [x] subtask 1
  - [x] subtask 2
- [ ] to do
  - [x] subtask 1
  - [ ] subtask 2
